# FLASK-DEPLOYMENT

Weather app hecha en flask corriendo sobre un deployment de kubernetes en microk8s. Basado en https://github.com/PrettyPrinted/weather_app_flask 

## Creacion del ambiente de kubernetes

Suponiendo que usted no tiene acceso a ningun ambiente de kubernetes eso no sera problema, levantaremos un cluster de un 1 solo nodo utilizando una maquina virtual (en nuestro caso ubuntu con 4gb de ram) y le instalaremos microk8s el cual sera nuestra plataforma donde desplegaremos nuestras aplicaciones. 

1. En esta guia supondremos que usted ya tiene una maquina virtual Ubuntu en la cual realizarara esta practica

2. El primer paso sera instalar snap con el siguiente comando:

```bash
sudo apt install snapd
```

3. Una vez installado snap, instalaremos microk8s:

```bash
sudo snap install microk8s --classic
```
4. Una vez instalado microk8s tendremos que hacer algunos cambios en la configuracion para poder usar los comandos de administracion "kubectl" sin necesidar de usar el usuario root.

```bash
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
microk8s status --wait-ready
```
>Sugerimos que agregue el siguiente alias a su archivo .bashrc en su home
> alias kubectl="microk8s kubectl"
>Una vez agregado, corra el comando source .bashrc desde su home para que se carguen los cambios

5. Una vez listo los pasos previos tendremos nuestro cluster casi listo, nos falta solamente habilitar algunas extensiones que necesitaremos para el correcto despliegue de nuestra aplicacion.

```bash
microk8s enable registry (lo necesitaremos para subir la imagen de nuestra aplicacion)
microk8s enable dns (lo necesitaremos para poder resolver el dns de la api del clima)
```
Y con esto damos por finalizada la instalacion y configuracion de nuestro micro cluster de kubernetes

## Creacion de la Imagen de nuestra aplicacion

1. Ahora nos concentraremos en la creacion de la imagen de nuestra aplicacion, es una imagen chica y sencilla por lo cual no nos llevara mucho. Parados en el path flask-deployment/docker/, correremos el siguiente comando para construir la imagen

```bash
docker build . -t localhost:32000/flask-actualizacion:latest
```
Este comando creara la imagen en el registro de microk8s para luego poder ser utilizada en el cluster.

2. Podemos verificar que se creo de forma correcta viendo nuestras imagenes con el comando:

```bash
docker images
```

3. Se puede probar la imagen sin necesidad de crear un pod usando docker:

```bash
docker run -p 5000:5000 --name flask-actualizacion localhost:32000/flask-actualizacion:latest
```

## Creacion del deployment y testeo de la imagen

1. Antes de crear el deployment, haremos un simple testeo de nuestra imagen para verificar que funciona correctamente, para ello es necesario crear un archivo (por ejemplo, flask-pod.yaml) con el siguiente contenido:

```bash
apiVersion: v1
kind: Pod
metadata: 
  name: flask-actualizacion
  namespace: default
  labels:
    app: flask-actualizacion
spec:
  containers:
    - image: localhost:32000/flask-actualizacion:latest
      name: flask-actualizacion
      ports:
        - containerPort: 5000
```
2. Luego crearemos un proxy para poder acceder desde la IP o dns de nuestra VM:

```bash
kubectl port-forward flask-pod 5000:5000 --address 0.0.0.0
```
3. Ya terminado esto podremos acceder a la ip de nuestra vm y al puerto 5000 donde veremos nuestro servicio levantado, se puede ver las distintas ciudades buscadas previamente (estas se guardan en la weather.db) y buscar nuevas ciudades en el buscador.

4. Una vez ya probada la imagen y confirmado su correcto funcionamiento, crearemos un volumen para asi poder hacer cambios en el html del servicio sin necesidad de volver a construir la imagen, para esto primero tendremos que crear el volumen que sera la asociacion entre un path fisico de nuestra vm y el volumen en si. Pero primero tendremos que crear el directorio que usaremos, en nuestro caso es en /var/flask/templates

```bash
sudo mkdir /var/flask/templates
```
```bash
kubectl apply -f kubernetes/flask-pv.yaml
```
5. Una vez creado nuestro volumen tendremos que hacer un claim con el siguiente comando:

```bash
kubectl apply -f kubernetes/flask-claim.yaml
```
6. Una vez creado nuestro volumen tendremos que asegurarnos de que exista el archivo html del template en el path de la VM que asociamos a la vm

```bash
sudo cp docker/templates/weather.html /var/flask/templates/
```
7. Ya con el template listo, podemos desplegar nuestro deployment, en nuestro caso elegimos tener 4 replicas 

```bash
kubectl apply -f kubernetes/flask-deployment.yaml
```
8. Por ultimo, asi como hicimos con el pod, crearemos un proxy para poder acceder a los pods desde la IP o DNS de nuestra virtual.

```bash
kubectl port-forward deployment/flask-deployment 5000:5000 --address 0.0.0.0
```
9. Y listo, ya tendremos nuestro servicio corriendo en 4 pods y siendo accesible desde la IP de la maquina virtual 
![image.png](./image.png)
